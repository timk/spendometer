﻿using SomRepositories;
using SpendOMeter.Domain;
using SpendOMeter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomRepositoriesRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                GetTotals();
                GetRecipients();
                CreateRecipient();
                CreatePayments();
                ReadPayments();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static void GetTotals()
        {
            UsersRepository repository = new UsersRepository();
            BudgetSummary budgetSummary = repository.GetBudgetSummary(1);
        }

        private static void CreateRecipient()
        {
            Recipient recipient = new Recipient()
            {
                Name = "Paco's Clam Shack"
            };
            RecipientsRepository repository = new RecipientsRepository();
            int id = repository.Add(recipient);
            Console.WriteLine($"New recipient - Id: {id}, Name: {recipient.Name}");
        }

        private static void GetRecipients()
        {
            RecipientsRepository repository = new RecipientsRepository();
            IEnumerable<Recipient> recipients = repository.Get();
            foreach (Recipient recipient in recipients)
            {
                Console.WriteLine($"Recipient: name: {recipient.Name}");
            }
        }

        private static void CreatePayments()
        {
            Payment payment = new Payment("Al's", new decimal(3.44), DateTime.Now);
            
            PaymentsRepository repository = new PaymentsRepository();
            repository.Add(payment);
        }

        private static void ReadPayments()
        {
            PaymentsRepository repository = new PaymentsRepository();
            IEnumerable<Payment> payments = repository.Get();
            foreach (Payment payment in payments)
            {
                Console.WriteLine($"Payment: amount: ${payment.Amount}");
            }
        }
    }
}
