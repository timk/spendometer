﻿using System.Web.Helpers;

namespace SpendOMeter.ViewModels
{
    public class ChartViewModel
    {
        public Chart Chart { get; set; }
    }
}