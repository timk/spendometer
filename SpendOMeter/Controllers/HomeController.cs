﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using SpendOMeter.ViewModels;
using SomRepositories;
using SpendOMeter.Domain;

namespace SpendOMeter.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            string clientDesc = "?";
            if(Request.Browser.IsMobileDevice)
            {
                clientDesc = "Mobile";
            }
            HomeViewModel viewModel = new HomeViewModel()
            {
                Client = clientDesc
            };
            return View(viewModel);
        }


        public ActionResult BudgetChartView()
        {
            ChartViewModel viewModel = new ChartViewModel()
            {
                Chart = BuildChart()
            };

            return View(viewModel);
        }
        

        private System.Web.Helpers.Chart BuildChart()
        {
            UsersRepository repository = new UsersRepository();
            BudgetSummary budgetSummary = repository.GetBudgetSummary(1);

            System.Web.Helpers.Chart chart = new System.Web.Helpers.Chart(400, 500, ChartTheme.Green)
             .AddTitle($"My Budget: ${budgetSummary.Amount}")
              //.AddLegend("Item1")
              .AddSeries
             (
                 chartType: "StackedColumn",
                    name: $"Spent: ${budgetSummary.Spent}",
                 xValue: new[] { "August, 2016" },
                 yValues: new[] { budgetSummary.Spent },
                 axisLabel: $"Spent {budgetSummary.Spent}"
             )
             .AddSeries(
             chartType: "StackedColumn",
             name: $"Remaining: ${budgetSummary.Remaining}",
             xValue: new[] { "August, 2016" },
             yValues: new[] { budgetSummary.Remaining },
             axisLabel: $"Remaining: ${budgetSummary.Remaining}"
             )
             .AddLegend();

            return chart;
            //    .AddSeries(chartType: "StackedBar",
            //        xValue: new[] { "2012", "2013", "2014", "2015" },
            //        yValues: new[] { 15, 5, 74, 32 }

            //)
            //.AddLegend()
            //.AddSeries(
            ////name: "WebSite",
            //chartType: "column",
            //xValue: new[] { "Digg", "DZone", "DotNetKicks", "StumbleUpon" },
            //yValues: new[] { "150000", "180000", "120000", "250000" })
            
            //return new System.Web.Helpers.Chart(600, 400, ChartTheme.Blue)
            //    .AddTitle("Number of website readers")
            //    .AddLegend()
            //    .AddSeries(
            //        name: "WebSite",
            //        chartType: "column",
            //        xValue: new[] { "Digg", "DZone", "DotNetKicks", "StumbleUpon" },
            //        yValues: new[] { "150000", "180000", "120000", "250000" });

        }
    }
}
