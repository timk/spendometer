﻿using SpendOMeter.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpendOMeter.Domain;
using SpendOMeter.Repositories;

namespace SpendOMeter.Controllers
{
    public class PaymentsController : SkateBaseController
    {
        // GET: Payments
        public ActionResult Index()
        {
            PaymentsViewModel viewModel = new PaymentsViewModel();
            viewModel.Paymnets = GetPayments();
            return View(viewModel);
        }

        private IEnumerable<Payment> GetPayments()
        {
            PaymentsRepository repository = new PaymentsRepository();
            IEnumerable<Payment> payments = repository.Get();
            //List<PaymentViewModel> retVal = new List<PaymentViewModel>();
            //// Use AutoMapper
            //foreach(Payment payment in payments)
            //{

            //}
            //return retVal;
            return payments;
        }

        // GET: Payments/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Payments/Create
        public ActionResult Create()
        {
            if (Request.UrlReferrer != null)
            {
                ViewBag.ReferringUrl = Request.UrlReferrer.ToString();
            }
            RecipientsRepository repository = new RecipientsRepository();
            ViewBag.Recipients = repository.Get();
            Payment payment = new Payment();
            return View(payment);
        }

        // POST: Payments/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            DateTime date = DateTime.Now;
            decimal amount = new decimal(0.0);
            int recipientId = 0;
            string name = "";
            try
            {
                amount = GetFormValue<decimal>(collection, "amount");
                name = GetFormValue<string>(collection, "name");
                date = GetFormValue<DateTime>(collection, "date");
                recipientId = GetFormValue<int>(collection, "recipientId");
                PaymentsRepository repository = new PaymentsRepository();
                Payment payment = new Payment()
                {
                    Amount = amount,
                    Name = name,
                    Date = date,
                    RecipientId = recipientId,
                    CategoryId = 1
                };
                repository.Add(payment);
                //return SkateRedirect(collection);
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                throw new Exception("Failed to add payment.", ex);
                //return View();
            }
        }

        // GET: Payments/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Payments/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Payments/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Payments/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
