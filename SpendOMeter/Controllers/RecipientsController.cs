﻿using SpendOMeter.Domain;
using SpendOMeter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SpendOMeter.Controllers
{
    public class RecipientsController : SkateBaseController
    {
        // GET: Recipients
        public ActionResult Index()
        {
            return View();
        }

        // GET: Recipients/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Recipients/Create
        public ActionResult Create()
        {
            if (Request.UrlReferrer != null)
            {
                ViewBag.ReferringUrl = Request.UrlReferrer.ToString();
            }
            return View();
        }

        // POST: Recipients/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            string name = "";
            try
            {
                if (collection["name"] == null)
                {
                    throw new Exception("The name must be specified.");
                }
                name = collection["name"];

                RecipientsRepository repository = new RecipientsRepository();
                Recipient recipient = new Recipient()
                {
                    Name = name
                };
                repository.Add(recipient);
                return SkateRedirect(collection);
            }
            catch(Exception ex)
            {
                throw new Exception("Failed to create new Recipient.", ex);
            }
        }



        // GET: Recipients/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Recipients/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Recipients/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Recipients/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
