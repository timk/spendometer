﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SpendOMeter.Controllers
{
    public class SkateBaseController : Controller
    {
        protected string GetReferringUrl(FormCollection collection)
        {
            if (collection["referringUrl"] != null)
            {
                return collection["referringUrl"];
            }
            return "";
        }

        protected ActionResult SkateRedirect(FormCollection collection)
        {
            if (!string.IsNullOrEmpty(this.GetReferringUrl(collection)))
            {
                return Redirect(this.GetReferringUrl(collection));
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        protected T GetFormValue<T>(FormCollection collection, string key)
        {
            if(collection[key] == null)
            {
                throw new Exception($"The key {key} was not present in the form.");
            }
            
            T retVal = (T)Convert.ChangeType(collection[key], typeof(T));
            return retVal;
        }

    }
}