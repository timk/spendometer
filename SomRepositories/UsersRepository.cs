﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpendOMeter.Domain;
using MicroLite.Configuration;
using MicroLite;

namespace SomRepositories
{
    public class UsersRepository : ISomRepository
    {
        public BudgetSummary GetBudgetSummary(int userId)
        {
            BudgetSummary retVal = null;
            try
            {
                var sessionFactory = Configure.Fluently().ForMsSql2012Connection("DefaultConnection").CreateSessionFactory();
                //           var sessionFactory = Configure
                //.Fluently()
                //.ForConnection(connectionName: "DefaultConnection",
                //    sqlDialect: "MicroLite.Dialect.SQLiteDialect"),
                //.CreateSessionFactory();
                //Configure.Extensions().WithConventionBasedMapping
                //    (
                //    new ConventionMappingSettings
                //    {
                //        ResolveTableName
                //    }
                //    );
                using (var session = sessionFactory.OpenSession())
                {
                    var query = new SqlQuery("select max(b.Amount) as 'Amount', sum(p.Amount) as 'Spent' from users u join Budgets b on b.userid = u.id left outer join Payments p on p.userid = u.id where u.id = 1 group by b.Id");
                    IList< BudgetSummary> budgets = session.Fetch<BudgetSummary>(query); // foos will be an IList<foo>
                    if(budgets.Count < 1)
                    {
                        throw new Exception($"Found {budgets.Count} budgets.  There should only ever be 1.");
                    }
                    retVal = budgets.First();      
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to query for Budget information.", ex);
            }
            return retVal;
        }
    }
}
