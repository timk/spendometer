﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpendOMeter.Domain;
using MicroLite.Configuration;
using MicroLite;

namespace SpendOMeter.Repositories
{
    public class RecipientsRepository
    {
        public IEnumerable<Recipient> Get()
        {
            List<Recipient> retVal = new List<Recipient>();
            try
            {
                var sessionFactory = Configure.Fluently().ForMsSql2012Connection("DefaultConnection").CreateSessionFactory();
                //           var sessionFactory = Configure
                //.Fluently()
                //.ForConnection(connectionName: "DefaultConnection",
                //    sqlDialect: "MicroLite.Dialect.SQLiteDialect"),
                //.CreateSessionFactory();
                using (var session = sessionFactory.OpenSession())
                {
                    var query = new SqlQuery("SELECT * from recipients");
                    var recipients = session.Fetch<Recipient>(query); // foos will be an IList<foo>
                    foreach (Recipient recipient in recipients)
                    {
                        retVal.Add(recipient);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return retVal;
        }

        public int Add(Recipient recipient)
        {
            int retVal = 0;

            // AutoMapper Payment to PaymentEntity

            var sessionFactory = Configure.Fluently().ForMsSql2012Connection("DefaultConnection").CreateSessionFactory();
            //           var sessionFactory = Configure
            //.Fluently()
            //.ForConnection(connectionName: "DefaultConnection",
            //    sqlDialect: "MicroLite.Dialect.SQLiteDialect"),
            //.CreateSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    session.Insert(recipient);
                    transaction.Commit();
                }
            }
            retVal = recipient.Id;
            return retVal;
        }
    }
}
