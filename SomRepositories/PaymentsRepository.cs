﻿using MicroLite;
using MicroLite.Configuration;
using MicroLite.Mapping;
using SpendOMeter.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;

namespace SpendOMeter.Repositories
{
    public class PaymentsRepository
    {
        public int Add(Payment payment)
        {
            // hack
            payment.UserId = 1;
            int retVal = 0;

            // AutoMapper Payment to PaymentEntity
            

            var sessionFactory = Configure.Fluently().ForMsSql2012Connection("DefaultConnection").CreateSessionFactory();
            //           var sessionFactory = Configure
            //.Fluently()
            //.ForConnection(connectionName: "DefaultConnection",
            //    sqlDialect: "MicroLite.Dialect.SQLiteDialect"),
            //.CreateSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    session.Insert(payment);
                    transaction.Commit();
                }
            }
            retVal = payment.Id;
            return retVal;
        }
        public IEnumerable<PaymentExtended> Get()
        {
            List<PaymentExtended> retVal = new List<PaymentExtended>();
            try
            {
                var sessionFactory = Configure.Fluently().ForMsSql2012Connection("DefaultConnection").CreateSessionFactory();
                //           var sessionFactory = Configure
                //.Fluently()
                //.ForConnection(connectionName: "DefaultConnection",
                //    sqlDialect: "MicroLite.Dialect.SQLiteDialect"),
                //.CreateSessionFactory();
                //Configure.Extensions().WithConventionBasedMapping
                //    (
                //    new ConventionMappingSettings
                //    {
                //        ResolveTableName
                //    }
                //    );
                using (var session = sessionFactory.OpenSession())
                {
                    var query = new SqlQuery("select * from Payments p join Recipients r on p.RecipientId = r.Id");
                    var payments = session.Fetch<PaymentExtended>(query); // foos will be an IList<foo>
                    foreach (PaymentExtended payment in payments)
                    {
                        retVal.Add(payment);
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Failed to query for payment information.", ex);
            }
            return retVal;
        } 
    }
}
