﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomRepositories
{
    public enum RepositoryType { Users, Payments, Recipients }
    public class RepositoryFactory
    {
        public static T GetRepository<T>(RepositoryType repositoryType)
        {
            object retVal = null;
            switch(repositoryType)
            {
                case RepositoryType.Users:
                    retVal =  new UsersRepository();
                    break;
                default:
                    throw new Exception($"Unsupported repository type: {repositoryType.ToString()}");
            }
            return (T)retVal;
        }
    }
}
