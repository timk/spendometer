﻿CREATE TABLE [dbo].[Budgets]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY, 
    [Amount] DECIMAL NOT NULL, 
    [StartMonth] DATE NOT NULL, 
    [EndMonth] DATE NULL, 
    [UserId] INT NOT NULL
)
