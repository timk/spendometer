﻿CREATE TABLE [dbo].[Payments]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY, 
    [RecipientId] INT NOT NULL, 
    [Amount] DECIMAL(18, 2) NOT NULL, 
	[Name] NVARCHAR(256) NOT NULL,
    [Date] DATETIME NOT NULL, 
    [CategoryId] INT NOT NULL,
	[UserId] INT NOT NULL
)
