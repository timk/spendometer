﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpendOMeter.Domain
{
    public class PaymentExtended : Payment
    {
        public string Name { get; set; }
    }
}
