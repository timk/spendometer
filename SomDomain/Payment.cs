﻿using MicroLite.Mapping.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpendOMeter.Domain
{
    public class Payment
    {
        // For ORM
        public Payment() { }
        public Payment(string recipientName, decimal amount, DateTime dateTime)
        {
            RecipientId = 3;// = new Recipient(recipientName);
            Amount = amount;
            Date = dateTime;
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }

        public int RecipientId { get; set; }
        
        public decimal Amount { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;
        public int CategoryId { get; set; }
    }
}
