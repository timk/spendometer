﻿namespace SpendOMeter.Domain
{
    public class Recipient : Person
    {

        /* for ORM */
        public Recipient() { }
        public Recipient(string recipientName)
        {
            this.Name = recipientName;
        }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}