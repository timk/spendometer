﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpendOMeter.Domain
{
    public class BudgetSummary
    {
        private decimal _spent;
        public decimal Amount { get; set; }
        public decimal Spent { get { return _spent; }
            set
            {
                if(value == null)
                {
                    _spent = new decimal(0.0);
                }
                else
                {
                    _spent = value;
                }
            }
        }
        public decimal Remaining
        {
            get
            { return this.Amount - this.Spent; }
        }
    }
}
